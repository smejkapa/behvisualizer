import argparse
import GmlToBeh
import os.path

if __name__ == '__main__':
    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='INPUT is either a single file or a directory to run recursively for all .beh files in it.', action='store')
    #parser.add_argument('-r', '--recursive', help='Insert refereced behaviors in place of BehaviorReference nodes?', action='store_true')
    args = parser.parse_args()

    if args.input:
        in_dir = args.input
        if not os.path.exists(in_dir):
            print('Specified path does not exist.')
            exit()
    else:
        in_dir = '.'
        print('Running in current directory')

    if in_dir.endswith('.beh_recursive.gml'):
        print('Beh trees composed from multiple files are not yet supported')
        exit()

    writer = GmlToBeh.GmlConverter()
    if os.path.isdir(in_dir):
        writer.run_dir(in_dir)
    else:
        writer.run_file(in_dir)
    
    print('XML files written')
