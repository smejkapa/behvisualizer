import networkx as nx
import xml.etree.ElementTree as Et
import NodeOptions as No
import os.path
import settings

order_single = -1


class BehXMLParser:
    def __init__(self, file, recursive, open_files, root_number=order_single):
        self.file = file
        self.recursive = recursive
        self.open_files = open_files
        self.to_skip = ['Behaviors']
        self.ref_tag = 'BehaviorReference'
        self.ref_attr = 'ReferenceName'
        self.graph = None
        self.group = ''
        self.root_number = root_number

    def process_file(self):
        root = Et.parse(self.file).getroot()
        self.graph = nx.DiGraph()
        root_id = '{}-{}'.format(self.file, root.tag)
        self.add_node(root_id, self.gen_label(root, self.root_number), root.tag, self.root_number)
        self.process_node(root, root_id)
        return self.graph, root_id

    def process_node(self, node, path):
        if len(node) == 1 and node[0].tag in self.to_skip:
            self.process_node(node[0], path)
            return
        i = order_single if len(node) == 1 else 1
        for c in node:
            self.process_child(c, path, i)
            i += 1

    def process_child(self, c, path, i):
        if self.recursive and c.tag == self.ref_tag:
            # Dealing with reference - go deeper
            ref_file = self.resolve_ref(c.attrib[self.ref_attr])
            if ref_file is not None and ref_file not in self.open_files:
                print('{}Following link: {}'.format(len(self.open_files) * '  ', ref_file))
                parser = BehXMLParser(ref_file, self.recursive, self.open_files.union({ref_file}), i)
                (g, g_root_id) = parser.process_file()
                self.graph = nx.compose(self.graph, g)
                self.graph.add_edge(path, g_root_id)
        else:
            c_path = '{}_{}{}'.format(path, i, c.tag)
            label = self.gen_label(c, i)
            self.add_node(c_path, label, c.tag, i)
            self.graph.add_edge(path, c_path)
            self.process_node(c, c_path)

    def resolve_ref(self, ref):
        directory = os.path.dirname(self.file)
        ref_file = os.path.join(directory, ref)
        ref_file += '.beh'
        if os.path.exists(ref_file):
            return ref_file
        else:
            print('Invalid link: {}'.format(ref_file))
            return None

    def gen_label(self, node, order):
        exclude = []
        label = '<html>'

        ord_string = ''
        if order != order_single and settings.write_order:
            ord_string = '({})'.format(order)

        # For references show the target
        # if node.tag == self.ref_tag:
        #    label += '{} [ref] <strong class="tag">{}</strong>'.format(ord_string, node.attrib[self.ref_attr])
        #    exclude = ['ReferenceName']
        # Default
        # else:
        label += '{} <strong class="tag">{}</strong>'.format(ord_string, node.tag)

        label += self.append_attribs(node, exclude)
        label += '</html>'
        return label

    @staticmethod
    def append_attribs(node, exclude=[]):
        attrs = ""
        for (k, a) in node.attrib.items():
            if k not in exclude:
                attrs += '<br /><span class="attrib">{} = {}</span>'.format(k, a)
        return attrs

    def add_node(self, node_id, label, tag, i):
        options = No.NodeOptions()
        if tag in No.node_options.keys():
            options = No.node_options[tag]

        size = options.size
        self.graph.add_node(node_id)
        self.graph.node[node_id]['label'] = node_id
        self.graph.node[node_id]['graphics'] = {
            'w': size['w'],
            'h': size['h'],
            # Move nodes somewhat to the right to preserve child order when layouting 
            'x': 100 * i,
            'fill': options.color,
            'type': options.shape
        }
        self.graph.node[node_id]['LabelGraphics'] = {
            'text': label,
            'fontSize': size['font'],
            'autoSizePolicy': 'content'
        }
