import xml.etree.ElementTree as Et
from custom_exceptions import InvalidGraphFormatError


class NodeData:
    attributes = dict()
    tag = ''

    def __init__(self, node):
        label_html = node['LabelGraphics']['text']
        label = Et.fromstring(label_html)

        # Get attributes of beh node from html attributes
        beh_attributes = [x for x in label.iter('span') if ('class', 'attrib') in x.attrib.items()]
        self.attributes = dict()
        for a in beh_attributes:
            split = a.text.split('=')
            self.attributes[split[0].strip()] = split[1].strip()

        # Find tag
        tag = [x for x in label.iter('strong') if ('class', 'tag') in x.attrib.items()]
        if len(tag) != 1:
            raise InvalidGraphFormatError('Missing tag in one of the graph nodes')
        self.tag = tag[0].text.strip()

    def to_element(self, parent=None):
        if parent is None:
            return Et.Element(self.tag, self.attributes)
        else:
            return Et.SubElement(parent, self.tag, self.attributes)
