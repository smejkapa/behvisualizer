from collections import namedtuple

default_size = dict([('w', 100), ('h', 50), ('font', 12)])

NodeOptions = namedtuple('NodeOptions', 'color size shape')
NodeOptions.__new__.__defaults__ = ('#fffbd1', default_size, 'rectangle')

node_options = {
    'Succeeder'         : NodeOptions(color = '#11dd11'),
    'Sequence'          : NodeOptions(color = '#9ba4ff', shape = 'parallelogram'),
    'OptionSelector'    : NodeOptions(color = '#ffbd95', shape = 'diamond'),
    'Selector'          : NodeOptions(color = '#ffbd95', shape = 'diamond'),
    'BehaviorReference' : NodeOptions(color = '#e9e9e9'),
    'Behavior'          : NodeOptions(color = '#d65aff'),
    'Inverter'          : NodeOptions(shape = 'triangle')
    }



