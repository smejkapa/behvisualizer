import argparse
from BehToyEd import BehConverter
import os.path
import settings

if __name__ == '__main__':
    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='INPUT is either a single file or a directory to run recursively for all .beh files in it.', action='store')
    parser.add_argument('-r', '--recursive', help='Insert refereced behaviors in place of BehaviorReference nodes?', action='store_true')
    parser.add_argument('-n', '--numbers', help='Insert an order number before node with multiple siblings?', action='store_true')
    args = parser.parse_args()

    if args.input:
        in_dir = args.input
        if not os.path.exists(in_dir):
            print('Specified path does not exist.')
            exit()
    else:
        in_dir = '.'
        print('Running in current directory')

    recursive = args.recursive
    settings.write_order = args.numbers

    # Run the converter
    converter = BehConverter()
    if os.path.isdir(in_dir):
        converter.run_dir(in_dir, recursive)
    else:
        converter.run_file(in_dir, recursive)
