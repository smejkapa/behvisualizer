import networkx as nx
from custom_exceptions import InvalidGraphFormatError
import xml.etree.ElementTree as Et
from xml.dom import minidom
from node_data import NodeData
import settings
import glob
import os


class GmlConverter:
    def __init__(self):
        self.graph = None
        self.beh_node = dict()
        self.beh_node['LabelGraphics'] = {'text': '<html> <strong class="tag">Behaviors</strong></html>'}
        # Nodes which can have 'Behaviors' node as their child
        self.beh_container_nodes = settings.behavior_composite_base_nodes

    def run_dir(self, directory, recursive=False):
        for file in glob.iglob(os.path.join(directory, '**', '*.beh.gml'), recursive=True):
            self.run_file(file, recursive)

    def run_file(self, file, recursive=False):
        print('Converting {} to XML'.format(file))
        self.graph = nx.readwrite.gml.read_gml(file, label='id')
        root_id = self.check_garph_format(self.graph)
        xml_root = NodeData(self.graph.node[root_id]).to_element()
        xml_root.attrib['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance'
        xml_root.attrib['xmlns:xsd'] = 'http://www.w3.org/2001/XMLSchema'

        self.process_node(root_id, xml_root)

        # Prettyprint xml
        raw_xml_string = Et.tostring(xml_root, 'utf-8')
        pretty_xml = minidom.parseString(raw_xml_string).toprettyxml(indent="  ")

        if file.endswith('.gml'):
            out_file = file[:-4]

        if not out_file.endswith('.beh'):
            out_file = out_file + '.beh'

        with open(out_file, 'w') as out_file_open:
            out_file_open.write(pretty_xml)

    def process_node(self, node_id, xml_parent):
        nodes = []
        for c_id in self.graph.successors(node_id):
            nodes.append((c_id, self.graph.node[c_id]))

        nodes.sort(key=lambda x: x[1]['graphics']['x'])

        # Insert 'Behaviors' node as a wrapper for nodes with multiple child nodes
        if len(nodes) > 0 and xml_parent.tag in self.beh_container_nodes:
            xml_parent = NodeData(self.beh_node).to_element(xml_parent)

        # Process nodes in order - lower x coordinate first
        for (n_id, n) in nodes:
            child_element = NodeData(n).to_element(xml_parent)
            self.process_node(n_id, child_element)

    @staticmethod
    def check_garph_format(graph):
        roots = [n for n, d in graph.in_degree() if d == 0]
        if len(roots) != 1:
            raise InvalidGraphFormatError('There has to be exactly one root node')
        if not nx.is_tree(graph):
            raise InvalidGraphFormatError('Specified graph has to be a tree')
        return roots[0]
