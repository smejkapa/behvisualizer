# Directly set here
behavior_composite_base_nodes = ['OptionSelector', 'Selector', 'Sequence']

# Controlled by command line
# A command line option will probably override these options
write_order = False
