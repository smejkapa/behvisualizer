import networkx as nx
import glob
import os.path
import BehXMLParser


class BehConverter:
    def __init__(self):
        pass

    def run_dir(self, directory: str, recursive=False):
        for file in glob.iglob(os.path.join(directory, '**', '*.beh'), recursive=True):
            self.run_file(file, recursive)

    @staticmethod
    def run_file(file, recursive=False):
        print('Processing: {}'.format(file))
        parser = BehXMLParser.BehXMLParser(file, recursive, {file})
        (graph, _) = parser.process_file()
        modifier = ''
        if recursive:
            modifier = '_recursive'
        nx.readwrite.gml.write_gml(graph, '{}{}.gml'.format(file, modifier))
        # nx.write_graphml(graph, '{}{}.graphml'.format(file, modifier))
