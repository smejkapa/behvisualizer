# Visualizer of behavior trees from Children of Galaxy #

## What you need ##

* [Python 3](https://www.python.org/downloads/) (Version 3.6.4, but others should work fine as well)
* `pip3 install networkx` (Version 2.1, but others should work fine as well)
* [yEd graph editor](https://www.yworks.com/products/yed) (Version 3.16.2.1, but others should work as well.)

**For latest version checkout the `develop` branch. For latest stable version checkout the `master` branch**

For versioned releases checkout commit from the `master` branch with according tag, e.g., v0.1 etc.

## How to use the app ##

### Graph generation ###

To generate a `.gml` graph files run `py -3 run_visualizer.py`. Graph files are generated to the directory with original `.beh` file.

By default the script recursively searches for `.beh` files in current directory.

**To see command line arguments run `py -3 run_visualizer.py --help`**

### Opening behavior tree in yEd ###

1. Open generated `.gml` graph file in yEd
1. Edit > Select All (Ctrl + A)
1. Tools > Fit Node to Label (min width 50, min height 30)
1. Layout > Tree > Directed (Alt + Shift + T)

### Editing behavior tree in yEd ###

To **import custom behavior tree nodes** to yEd:

1. Edit > Palette Manager > Import Section
1. Navigate to `[this repository folder]/yEd Palette/`
1. Select `CoG Behavior`

There is nothing special about the nodes. You just need to make sure you edit the HTML of them correctly. HTML class `tag` corresponds to XML element tag. HTML class `attrib` corresponds to XML attributes of given element. (Currently these also need to be with HTML tags strong and span respectively.)
When editing make sure to order your nodes as you want them to execute from left to right.

After you save the graph as GML again you can **export it back to the behavior tree XML** format.

1. Save as GML
1. Run `py -3 run_writer.py -i EDITED_GRAPH_FILE`

To see all command line arguments use `--help` just like above.

**Note:** saving recursive behaviors is not supported. E.g., you can have reference nodes in your files but you cannot have them expanded. That means the `writer` can save whatever the `visualizer` *without* the `-r` option can save. Writer can save only a single file at a time.
